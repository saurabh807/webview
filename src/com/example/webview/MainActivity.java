package com.example.webview;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.webview.MyTestService;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		
		Button saveme=(Button)findViewById(R.id.save);
		 
        saveme.setOnClickListener(new Button.OnClickListener(){

            public void onClick(View v)
            {
                try{
                	
                	System.out.println("in click");
                	Intent i = new Intent(getApplicationContext(), MyTestService.class);
            		startService(i);
                	System.out.println("in click");
                         // CALL GetText method to make post method call
                      
                 }
                catch(Exception ex)
                 {
                    System.out.println(ex.getMessage());
                 }
            }
        });  
    
		
		WebView myWebView = (WebView) findViewById(R.id.webView1);
		myWebView.setWebViewClient(new MyBrowser());
		myWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		//to enable JavaScript
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		//myWebView.setWebChromeClient(new WebChromeClient());
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
	    if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
	        fixNewAndroid(myWebView);
	    }
		myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
		String url="http://www.ebeedu.com";
		myWebView.loadUrl(url);
		
		
		
		
	}
	
	@TargetApi(16)
	protected void fixNewAndroid(WebView webView) {
	    try {
	        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
	    } catch(NullPointerException e) {
	    }
	}
	
//Binding JavaScript code to Android code
	public class WebAppInterface {
	    Context mContext;

	    /** Instantiate the interface and set the context */
	    WebAppInterface(Context c) {
	        mContext = c;
	    }

	    /** Show a toast from the web page */
	    @JavascriptInterface
	    public void showToast(String toast) {
	    	//MyTestService mservice=new MyTestService();
	    	
	        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	    }
	}
	
	private class MyBrowser extends WebViewClient {
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	  if (Uri.parse(url).getHost().equals("http://www.ebeedu.com")) {
	              // This is my web site, so do not override; let my WebView load the page
	              return false;
	          }
	          // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
	          Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	          startActivity(intent);
	          return true;
	   }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	
	
	int counter;
	

}
